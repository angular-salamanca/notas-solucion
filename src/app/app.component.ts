import { Component, OnInit } from '@angular/core';
import { MensajeroService } from './core/mensajero.service';
import { TemasMensajes } from './core/modelo/temas-mensajes.enum';
import { Mensajes } from './core/modelo/mensajes.enum';

@Component({
  selector: 'notas-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public cargando = false;

  constructor(private mensajero: MensajeroService) { }

  ngOnInit() {
    this.mensajero.escuchar(TemasMensajes.alumnos).subscribe(
      mensaje => {
        if (mensaje.contenido === Mensajes.cargando) {
          this.cargando = true;
        } else if (mensaje.contenido === Mensajes.cargados) {
          this.cargando = false;
        }
      }
    );
  }
}
