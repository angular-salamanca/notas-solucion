import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { AulaComponent } from './aula/aula.component';
import { EstadisticasModule } from '../estadisticas/estadisticas.module';
import { AlumnosModule } from '../alumnos/alumnos.module';

@NgModule({
  declarations: [CabeceraComponent, AulaComponent],
  imports: [
    CommonModule,
    EstadisticasModule,
    AlumnosModule
  ],
  exports: [CabeceraComponent, AulaComponent]
})
export class CoreModule { }
