import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Mensaje } from './modelo/mensaje.interface';
import { filter } from 'rxjs/operators';
import { TemasMensajes } from './modelo/temas-mensajes.enum';

@Injectable({
  providedIn: 'root'
})
export class MensajeroService {

  private mensajero = new BehaviorSubject<Mensaje>(null);
  private mensajero$ = this.mensajero.asObservable().pipe(
    filter(mensaje => mensaje !== null)
  );

  constructor() { }

  public emitir(mensaje: Mensaje) {
    this.mensajero.next(mensaje);
  }

  public escuchar(tema: TemasMensajes) {
    return this.mensajero$.pipe(
      filter(
        mensaje => mensaje.tema === tema
      )
    );
  }
}
