import { TemasMensajes } from './temas-mensajes.enum';
import { Mensajes } from './mensajes.enum';

export interface Mensaje {
  tema: TemasMensajes;
  contenido: Mensajes;
}
