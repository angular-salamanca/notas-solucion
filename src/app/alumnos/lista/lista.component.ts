import { Component, OnInit, Input } from '@angular/core';
import { AlumnosService } from 'src/app/core/alumnos.service';
import { Alumno } from '../modelo/alumno.interface';

@Component({
  selector: 'notas-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  @Input()
  aula: string;

  public alumnos: Alumno[] = [];

  constructor(private alumnosService: AlumnosService) { }

  public onBorrarAlumno(e: Event, alumno: Alumno) {
    e.preventDefault();
    this.alumnosService.borrarAlumno(alumno.id);
  }

  ngOnInit() {
    this.alumnosService.alumnos$.subscribe(
      alumnos => this.alumnos = alumnos.filter(alumno => alumno.aula.toLowerCase() === this.aula.toLowerCase())
    );
  }

}
