import { Component, OnInit, Input } from '@angular/core';
import { AlumnosService } from 'src/app/core/alumnos.service';
import { Alumno } from 'src/app/alumnos/modelo/alumno.interface';

@Component({
  selector: 'notas-estadisticas-aula',
  templateUrl: './estadisticas-aula.component.html',
  styleUrls: ['./estadisticas-aula.component.css']
})
export class EstadisticasAulaComponent implements OnInit {

  @Input()
  public aula: string;

  public alumnos: Alumno[] = [];
  public resultados = {
    media: 0,
    maxima: 0,
    aprobados: {
      totales: 0,
      porcentaje: 0
    },
    suspensos: {
      totales: 0,
      porcentaje: 0
    }
  };
  public media: number;
  public maxima: number;

  constructor(private alumnosService: AlumnosService) { }

  ngOnInit() {
    this.alumnosService.alumnos$.subscribe(
      alumnos => {
        this.alumnos = alumnos.filter(alumno => alumno.aula.toLowerCase() === this.aula.toLowerCase());
        this.resultados.media = this.getNotaMedia();
        this.resultados.maxima = this.getNotaMaxima();
        this.resultados.aprobados.totales = this.getAprobados();
        this.resultados.aprobados.porcentaje = this.getPorcentajeAprobados();
        this.resultados.suspensos.totales = this.getSuspensos();
        this.resultados.suspensos.porcentaje = this.getPorcentajeSuspensos();
      });
  }

  private getNotaMedia(): number {
    if (this.alumnos.length === 0) {
      return 0;
    } else {
      return this.alumnos.reduce((acc, alumno) => acc + alumno.nota, 0) / this.alumnos.length;
    }
  }

  private getNotaMaxima(): number {
    return this.alumnos.length === 0 ? 0 : this.alumnos.reduce((acc, alumno) => acc >= alumno.nota ? acc : alumno.nota, 0);
  }

  private getAprobados(): number {
    return this.alumnos.filter(alumno => alumno.nota >= 5).length;
  }

  private getPorcentajeAprobados(): number {
    return (this.resultados.aprobados.totales / this.alumnos.length) * 100;
  }

  private getSuspensos(): number {
    return this.alumnos.length - this.resultados.aprobados.totales;
  }

  private getPorcentajeSuspensos(): number {
    return (this.resultados.suspensos.totales / this.alumnos.length) * 100;
  }

  getClasesCara(nota: number) {
    return {
      'fa-frown': nota < 5,
      'fa-smile': nota >= 5,
      'icono-ok': nota >= 5,
      'icono-no-ok': nota < 5
    };
  }

}
